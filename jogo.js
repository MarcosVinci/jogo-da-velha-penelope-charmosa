// SCRIPT DO JOGO DA VELHA //

var jogador = 0; // Jogador de início é a Penélope
var jogadas = 0; // Quantidade de jogadas até se ter o limite 9


function clique(id) {
	console.log("jogador: "+jogador);

	var tag = null;

	if(id.currentTarget)
		tag = id.currentTarget; // "currentTarget" obtém o elemento alvo do evento, no caso, o clique.
	else
		tag = document.getElementById('casa'+id);

	if(tag.style.backgroundImage == '' || tag.style.backgroundImage == null) {
		var endereco = 'img/'+jogador+'.jpg';
		tag.style.backgroundImage = 'url('+endereco+')';
		jogadas += 1;

		if(jogadas>=5) {
			var ganhou = verificarGanhador(tag);
			comunicados = document.getElementById('comunicados');
			switch(ganhou) // Casos de verificação para ganhador ou empate.
			{
				case 1:
					if(jogador==0)
						comunicados.innerHTML = 'Ufa! Penélope escapou!';
					else
						comunicados.innerHTML = 'Oh não! Tião Gavião capturou Penélope!';
					break;
				case -1:
					comunicados.innerHTML = 'Empate!! A Quadrilha de Morte está a caminho!';
					break;
				case 0:
					// Não há vencedores no momento.
					break;
			}

			if(ganhou!=0)
			{
				finalizar();
				document.getElementById('novamente').style.display = 'inline';
				vez.innerHTML = '';
				if(start)
					iniciar()
			}
		};

		// Definindo vez do jogador.
		vez = document.getElementById('vez');
		if(jogador==0)
		{
			jogador = 1;
			vez.innerHTML = 'Tião Gavião';
		}else
		{
			jogador = 0;
			vez.innerHTML = 'Penélope Charmosa';
		}


	}
}

// Função de verificação de jogador atualizada e reduzida.
function verificarGanhador(jogada) {
	const possibilidades = {
    'casa1': [[1, 2, 3], [1, 5, 9], [1, 4, 7]],
    'casa2': [[1, 2, 3], [2, 5, 8]],
    'casa3': [[1, 2, 3], [7, 5, 3], [3, 6, 9]],
    'casa4': [[4, 5, 6], [1, 4, 7]],
    'casa5': [[4, 5, 6], [1, 5, 9], [3, 5, 7], [2, 5, 8]],
    'casa6': [[4, 5, 6], [3, 6, 9]],
    'casa7': [[7, 8, 9], [3, 5, 7], [1, 4, 7]],
    'casa8': [[7, 8, 9], [2, 5, 8]],
    'casa9': [[7, 8, 9], [1, 5, 9], [3, 6, 9]],
  }; // Todas as possibilidades de vitória para cada casa
	let casa = jogada.id;
	var possibilidade = possibilidades[casa];

	for(let pos = 0; pos < possibilidade.length; pos++) {
		let [casaA, casaB, casaC] = possibilidade[pos];
		let c1 = document.getElementById(`casa${casaA}`).style.backgroundImage;
		let c2 = document.getElementById(`casa${casaB}`).style.backgroundImage;
		let c3 = document.getElementById(`casa${casaC}`).style.backgroundImage;

		if(c1==c2 && c1==c3 && c1!='') {
			return 1; // Para 1, se tem o vencedor.
		};
	};

	if(jogadas==9) {
		return -1;
	};

	return 0;
}

// Final de jogo "zerando" o jogo anterior para um novo.
function finalizar() {
	tags = document.getElementsByClassName('casa');

	for(i=0; i<9; i++) {
		tags[i].onclick = null;
	}

}

// Função de início de jogo, indicando a vez dos jogadores
function iniciar() {
	jogador = jogador == 0 ? 1 : 0;
	jogadas = 0;

	casas = document.getElementsByClassName('casa');

	for(i=0; i<9; i++) {
		casas[i].onclick=clique;
		casas[i].style.backgroundImage='';
	}

	document.getElementById('novamente').style.display = 'none';

	vez.innerHTML = jogador == 0 ? 'Penélope Charmosa' : 'Tião Gavião';

	comunicados = document.getElementById('comunicados');
	comunicados.innerHTML = '';

}
